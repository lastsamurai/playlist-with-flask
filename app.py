#!/usr/bin/env python3
from flask import Flask, request, redirect, render_template
from random import randint

app= Flask(__name__)


@app.route('/',methods=['POST','GET','DELETE'])
def hello():
    if request.method=='POST': #CREATE
        obj={
                "title":request.form['title'],
                "artist":request.form['artist'],
                "album":request.form['album']
            }
        
        try:
            fhandle=open('datastore.txt','a+')
        except:
            return "Could not open datastore file"
        
        newId=randint(100,10000)

        fhandle.write("{};{};{};{}\n".format( newId , obj['title'],obj['artist'],obj['album']))

        fhandle.close()
        
        return redirect('/')

    else:   # RETRIEVE

        try:
            fhandle=open('datastore.txt','r')
        except:
            return "could not open datastore file"

        playlist=[]
        for line in fhandle:
            track=line.split(';')
            obj={
                "id":track[0],
                "title":track[1],
                "artist":track[2],
                "album":track[3]
            }
            playlist.append(obj)
        fhandle.close()

        return render_template('index.html',playlist=playlist)

@app.route('/delete/<int:id>',methods=['POST'])
def delete(id):

    try:
        fhandle=open('datastore.txt')
    except:
        return "could not open datastore file"
    
    newContent=""

    for line in fhandle:
        track=line.split(';')
        if int(track[0]) ==id :
            continue
        newContent+=line

    fhandle.close()
    try:
        fhandle=open('datastore.txt','w')
    except:
        return "could not open datastore file"
    fhandle.write(newContent)
    fhandle.close()

    return redirect('/')

@app.route('/update/<int:id>',methods=['POST','GET'])
def update(id):
    obj={
        "id":id
    }
    if request.method=='POST':
        
        try:
            fhandle=open('datastore.txt')
        except:
            return "could not open datastore file"

        obj['title']=request.form['title']
        obj['artist']=request.form['artist']
        obj['album']=request.form['album']
        
        newLine="{};{};{};{}\n".format( obj['id'],obj['title'],obj['artist'],obj['album'] )

        newContent=""

        for line in fhandle:
            track=line.split(';')
            if int(track[0]) ==id :
                newContent+= newLine
            else:
                newContent+=line

        fhandle.close()

        try:
            fhandle=open('datastore.txt','w')
        except:
            return "could not open datastore file"

        fhandle.write(newContent)
        fhandle.close()
        
        return redirect('/')

    else:

        try:
            fhandle=open('datastore.txt')
        except:
            return "could not open datastore file"

        for line in fhandle:
            track=line.split(';')
            if int(track[0]) ==id :
                l=line.split(';')
                obj["title"]=l[1]
                obj["artist"]=l[2]
                obj["album"]=l[3]
                break                

        fhandle.close()
        
        return render_template('edit.html',track=obj)


if __name__=="__main__":
    app.run(debug=True)